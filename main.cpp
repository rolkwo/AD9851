/*
 * main.cpp
 *
 *  Created on: Jun 8, 2017
 *      Author: roland
 */

#include <Arduino.h>

#include <AD9851.h>

AD9851 ad9851(9, 10, 8, 7);

void setup()
{
	ad9851.setFrequency(50450000);
//	for(int i = 0; i < 1000; ++i)
		ad9851.sync();
}

void loop()
{
	delay(1000);
	ad9851.setFrequency(50450100);
//	ad9851.setPowerDown(true);
	ad9851.sync();
	delay(1000);
	ad9851.setFrequency(50450000);
//	ad9851.setPowerDown(false);
	ad9851.sync();
}
