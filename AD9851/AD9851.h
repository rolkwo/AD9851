/*
 * AD9851.h
 *
 *  Created on: Jun 7, 2017
 *      Author: roland
 */

#ifndef LIB_AD9851_AD9851_H_
#define LIB_AD9851_AD9851_H_

#include <Arduino.h>

class AD9851
{
public:
	AD9851(uint8_t wclk, uint8_t data, uint8_t fq, uint8_t reset);

	void sync();
	void setFrequency(uint32_t freq);
	void setPhase(uint16_t phase);
	void setPowerDown(bool powerDown);

private:
	uint8_t _wclk;
	uint8_t _data;
	uint8_t _fq;
	uint8_t _reset;

	uint32_t _freq;
	uint16_t _phase;
	bool _powerDown;

	void sendData(uint32_t byte, uint8_t len);
	void sendOne();
	void sendZero();
	void sendReset();
	void initSerialComm();
};


#endif /* LIB_AD9851_AD9851_H_ */
