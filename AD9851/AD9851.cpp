#include <AD9851.h>

AD9851::AD9851(uint8_t wclk, uint8_t data, uint8_t fq, uint8_t reset)
	: _wclk(wclk), _data(data), _fq(fq), _reset(reset), _freq(0), _phase(0), _powerDown(false)
{
	  pinMode(_wclk, OUTPUT);
	  pinMode(_data, OUTPUT);
	  pinMode(_fq, OUTPUT);
	  pinMode(_reset, OUTPUT);
	  sendReset();
	  initSerialComm();
}

void AD9851::sync()
{
	  digitalWrite (_fq, LOW);

	  uint32_t toSend = (_freq * pow(2, 32)) / 180000000;
	  sendData(toSend, 32);
	  sendData(1, 1);	//X6 mul
	  sendData(0, 1);	//dummy zero
	  sendData(_powerDown, 1);
	  sendData(_phase, 5);



//	  sendData((_phase << 3) | (_powerDown << 2) | 0x01, 8);

	  digitalWrite (_fq, HIGH);
}

void AD9851::setFrequency(uint32_t freq)
{
	_freq = freq;
}

void AD9851::setPhase(uint16_t phase)
{
	_phase = phase & 0x1f;
}

void AD9851::setPowerDown(bool powerDown)
{
	_powerDown = powerDown;
}

void AD9851::sendData(uint32_t byte, uint8_t len)
{
	  for(uint8_t i = 0; i < len; i++)
	  {
	    if(byte & 1)
	      sendOne();
	    else
	      sendZero();
	    byte >>= 1;
	  }
}

void AD9851::sendOne()
{
	  digitalWrite(_wclk, LOW);
	  digitalWrite(_data, HIGH);
	  digitalWrite(_wclk, HIGH);
	  digitalWrite(_data, LOW);
}

void AD9851::sendZero()
{
	  digitalWrite(_wclk, LOW);
	  digitalWrite(_data, LOW);
	  digitalWrite(_wclk, HIGH);
}

void AD9851::sendReset()
{
	digitalWrite(_reset, HIGH);
//	delay(100);
	digitalWrite(_reset, LOW);
}

void AD9851::initSerialComm()
{
	//please note that pins D0 and D1 should be connected to VCC, while D2 should be short to GND
	digitalWrite(_wclk, HIGH);
	digitalWrite(_wclk, LOW);
	digitalWrite(_fq, HIGH);
	digitalWrite(_fq, LOW);
}

